FROM nginxinc/nginx-unprivileged:stable-alpine
LABEL maintainer="Sundar M"
COPY ./default.conf.tpl /etc/nginx/default.conf.tpl
COPY ./uwsgi_params /etc/nginx/uwsgi_params

ENV LISTEN_PORT=8000
ENV APP_HOST=App
ENV APP_PORT=900

USER root

RUN mkdir -p /vol/static
RUN chmod -R 755 /vol/static
RUN touch /etc/nginx/conf.d/default.conf
RUN chown nginx:nginx  /etc/nginx/conf.d/default.conf
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

USER nginx

CMD ["/entrypoint.sh"]

